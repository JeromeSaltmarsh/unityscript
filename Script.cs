using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public enum OnScriptFinished
{
    Do_Nothing,
    Destroy_Behavior,
    Disable_Behavior,
    Destroy_GameObject,
    Deactivate_GameObject,
    Loop
}

public class Script
{
    private enum Channel
    {
        Update,
        Fixed_Update
    }
    
    private class GameActionChain : MonoBehaviour
    {
        public List<Func<bool>> actions = new List<Func<bool>>();
        public OnScriptFinished onScriptFinished;
        public int index;
        public Channel channel = Channel.Fixed_Update;

        private void FixedUpdate()
        {
            if (channel == Channel.Fixed_Update)
            {
                run();
            }
        }

        private void Update()
        {
            if (channel == Channel.Update)
            {
                run();
            }
        }

        private void run()
        {
            for (; index < actions.Count; index++)
            {
                if (!actions[index].Invoke()) return;
            }

            switch (onScriptFinished)
            {
                case OnScriptFinished.Destroy_Behavior:
                    Destroy(this);
                    return;
                case OnScriptFinished.Disable_Behavior:
                    enabled = false;
                    return;
                case OnScriptFinished.Deactivate_GameObject:
                    gameObject.SetActive(false);
                    return;
                case OnScriptFinished.Destroy_GameObject:
                    Destroy(gameObject);
                    return;
                case OnScriptFinished.Loop:
                    index = 0;
                    return;
                case OnScriptFinished.Do_Nothing:
                    return;
            }
        }
    }
    
    public string name => actionChain.name;
    public GameObject gameObject => actionChain.gameObject;
    private readonly GameActionChain actionChain;
    private static GameObject scripts;
    
    public Script(
        GameObject gameObject, 
        OnScriptFinished onScriptFinished = OnScriptFinished.Destroy_Behavior, 
        bool newGameActionChain = true)
    {
        actionChain = newGameActionChain ? gameObject.AddComponent<GameActionChain>() : gameObject.GetOrAdd<GameActionChain>();
        actionChain.onScriptFinished = onScriptFinished;
    }

    public void setOnFinished(OnScriptFinished value)
    {
        actionChain.onScriptFinished = value;
    }

    public Script(string name, OnScriptFinished onScriptFinished = OnScriptFinished.Deactivate_GameObject, Transform parent = null)
    {
        actionChain = new GameObject("Script - " + name).AddComponent<GameActionChain>();
        actionChain.onScriptFinished = onScriptFinished;

        if (parent == null)
        {
            if (scripts == null)
            {
                scripts = new GameObject("Scripts");
            }

            actionChain.transform.parent = scripts.transform;
        }
        else
        {
            actionChain.transform.parent = parent;
        }
    }

    public Script performIf(Func<bool> condition, Action action, Action elseAction = null)
    {
        return perform(delegate
        {
            if (condition())
            {
                action();
            }
            else
            {
                elseAction?.Invoke();
            }
        });
    }

    public Script performIf(bool condition, Action action, Action elseAction = null)
    {
        return perform(delegate
        {
            if (condition)
            {
                action();
            }
            else
            {
                elseAction?.Invoke();
            }
        });
    }

    public Script performFor(Action action, float duration)
    {
        float endTime = 0;
        return perform(() => endTime = Time.time + duration)
            .performUntil(action, () => Time.time > endTime);
    }
    
    public Script performFor(Action action, int times)
    {
        for (int i = 0; i < times; i++)
        {
            perform(action);
        }

        return this;
    }

    public Script async(Action<Script> asyncScript, string name = "Async")
    {
        return perform(delegate { asyncScript(new Script(name, OnScriptFinished.Destroy_GameObject)); });
    }

    public Script perform(Action action)
    {
        return add(
            delegate
            {
                action();
                return true;
            }
        );
    }

    public Script performUntil(Action action, Func<bool> condition)
    {
        return add(delegate
        {
            if (condition())
            {
                return true;
            }

            action();
            return false;
        });
    }

    public Script waitUntil(Func<bool> condition)
    {
        return add(condition);
    }

    public void deactivateActionChain()
    {
        perform(() => actionChain.gameObject.SetActive(false));
    }

    public void destroyActionChain()
    {
        perform(() => GameObject.Destroy(actionChain));
    }

    protected Script add(Func<bool> action)
    {
        actionChain.actions.Add(action);
        return this;
    }

    public Script setChannelUpdate()
    {
        return setChannel(Channel.Update);
    }

    public Script setChannelFixedUpdate()
    {
        return setChannel(Channel.Fixed_Update);
    }

    private Script setChannel(Channel channel)
    {
        return perform(() => actionChain.channel = channel);
    }

    public void loop()
    {
        actionChain.onScriptFinished = OnScriptFinished.Loop;
        setActionIndex(0);
    }
    
    public Script setActionIndex(int value)
    {
        return perform(delegate { actionChain.index = value; });
    }

    public void destroy()
    {
        GameObject.Destroy(actionChain);
    }
}

public static class ScriptExtensionsCore
{
     public static Script animateRotate(this Script script, Component component, float value, float duration, Ease ease = Ease.Linear)
    {
        return script.tween(
            () => component.transform.eulerAngles.z,
            () => component.transform.eulerAngles.z + value,
            duration: duration,
            angle =>
            {
                Vector3 eulerAngles =component.transform.eulerAngles;
                eulerAngles.z = angle;
                component.transform.eulerAngles = eulerAngles;
            }, ease);
    }
    
    public static Script tween(this Script script, Func<float> startValue, Func<float> endValue, float duration, Action<float> tweenFunction, Ease ease = Ease.Linear)
    {
        float startTime = 0;
        float endTime = 0;
        float startingValue = 0;
        float endingValue = 0;
        EasingFunction.Function easeFunction = EasingFunction.GetEasingFunction(ease);

        return script.perform(delegate
            {
                startTime = Time.time;
                endTime = startTime + duration;
                startingValue = startValue();
                endingValue = endValue();
            }).performUntil(delegate
            {
                float timeElapsed = Time.time - startTime;
                float easeValue = timeElapsed / duration;
                tweenFunction(easeFunction(startingValue, endingValue, easeValue));
            }, () => Time.time >= endTime)
            .perform(delegate
            {
                tweenFunction(easeFunction(startingValue, endingValue, 1f));
            });
    }
    
    public static Script remove(this Script script, Component component)
    {
        return script.perform(() => GameObject.Destroy(component));
    }
    
    public static Script remove<T>(this Script script, Component component) where T : Component
    {
        return script.perform(delegate
        {
            T t = component.GetComponent<T>();
            if (t != null)
            {
                GameObject.Destroy(t);
            }
        });
    }
    
    public static Script remove<T>(this Script script, GameObject gameObject) where T : Component
    {
        return script.perform(delegate
        {
            T t = gameObject.GetComponent<T>();
            if (t != null)
            {
                GameObject.Destroy(t);
            }
        });
    }
    
    public static Script setSprite(this Script script, GameObject gameObject, Sprite sprite)
    {
        return script.setSprite(gameObject.GetComponent<SpriteRenderer>(), sprite);
    }
    
    public static Script setSprite(this Script script, Component component, Sprite sprite)
    {
        return script.setSprite(component.GetComponent<SpriteRenderer>(), sprite);
    }

    public static Script setSprite(this Script script, SpriteRenderer spriteRenderer, Sprite sprite)
    {
        return script.perform(() => spriteRenderer.sprite = sprite);
    }

    public static Script destroy(this Script script, GameObject target)
    {
        return script.perform(() => GameObject.Destroy(target));
    }
    
    public static Script setActive(this Script script, Component component)
    {
        return script.setActive(component.gameObject);
    }

    public static Script setActive(this Script script, GameObject gameObject, bool value = true)
    {
        return script.perform(() => gameObject.SetActive(value));
    }

    public static Script setDeactive(this Script script, Component component)
    {
        return script.setDeactive(component.gameObject);
    }
    
    public static Script setOpacity(this Script script, float value)
    {
        return script.setOpacity(script.gameObject.GetComponent<SpriteRenderer>(), value);
    }
    
    public static Script setOpacity(this Script script, GameObject component, float value)
    {
        return script.setOpacity(component.GetComponent<SpriteRenderer>(), value);
    }

    public static Script setOpacity(this Script script, Component component, float value)
    {
        return script.setOpacity(component.GetComponent<SpriteRenderer>(), value);
    }
    
    public static Script setOpacity(this Script script, SpriteRenderer spriteRenderer, float value)
    {
        return script.perform(delegate
        {
            Color color = spriteRenderer.color;
            color.a = value;
            spriteRenderer.color = color;    
        });
    }
    
    public static Script setOpacity(this Script script, Graphic graphic, float value)
    {
        return script.perform(delegate
        {
            Color color = graphic.color;
            color.a = value;
            graphic.color = color;    
        });
    }

    public static Script setDeactive(this Script script, GameObject gameObject)
    {
        return script.setActive(gameObject, false);
    }

    public static Script wait(this Script script, float duration)
    {
        float endTime = 0;
        return script
            .perform(() => endTime = Time.time + duration)
            .waitUntil(() => Time.time >= endTime);
    }
    
    public static Script log(this Script script, string value)
    {
        return script.perform(() => Debug.Log("Script " + script.name + " : " + value));
    }   
    
    public static Script enableCollider(this Script script, GameObject gameObject)
    {
        return script.perform(() => gameObject.GetComponent<Collider>().enabled = true);
    }
    
    public static Script enableCollider(this Script script, Component component)
    {
        return script.perform(() => component.GetComponent<Collider>().enabled = true);
    }

    public static Script disableCollider(this Script script, GameObject gameObject)
    {
        return script.perform(() => gameObject.GetComponent<Collider>().enabled = false);
    }
    
    public static Script disableCollider(this Script script, Component component)
    {
        return script.perform(() => component.GetComponent<Collider>().enabled = false);
    }
    
    public static Script disable<T>(this Script script, Component target) where T : MonoBehaviour
    {
        return script.perform(delegate
        {
            T t = target.GetComponent<T>();
            if (t != null)
            {
                t.enabled = false;
            }
        });
    }
    
    public static Script animateOpacity(this Script script, Object objekt, float targetOpacity, float duration = 1f,
        Ease ease = Ease.Linear, bool childrenToo = true)
    {
        if (childrenToo)
        {
            script.perform(delegate
            {
                foreach (Transform child in GetTransform(objekt))
                {
                    script.async(asyncScript => asyncScript.animateOpacity(child, targetOpacity, duration, ease, childrenToo));
                }
            });
        }

        float startTime = 0;
        float endTime = 0;
        float startOpacity = 0;
        float value = 0;
        EasingFunction.Function easeFunction = EasingFunction.GetEasingFunction(ease);

        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>(objekt);
        if (spriteRenderer != null)
        {
            Color color = spriteRenderer.color;
            script.perform(delegate
            {
                color = spriteRenderer.color;
                startOpacity = color.a;
                startTime = Time.time;
                endTime = startTime + duration;
            }).performUntil(() =>
                {
                    float timeElapsed = Time.time - startTime;
                    color.a = easeFunction(startOpacity, targetOpacity, timeElapsed / duration);
                    spriteRenderer.color = color;
                },
                () => Time.time >= endTime);
        }
        else
        {
            Graphic graphic = GetComponent<Graphic>(objekt);
            if (graphic != null)
            {
                Color color = graphic.color;
                script.perform(() =>
                {
                    startTime = Time.time;
                    endTime = startTime + duration;
                    color = graphic.color;
                    startOpacity = color.a;
                }).performUntil(delegate
                    {
                        float timeElapsed = Time.time - startTime;
                        color.a = easeFunction(startOpacity, targetOpacity, timeElapsed / duration);
                        graphic.color = color;
                    },
                    () => Time.time >= endTime);
            }
        }

        return script;
    }

    private static Transform GetTransform(Object obj)
    {
        if (obj is GameObject gameObject)
        {
            return gameObject.transform;
        }
        return ((Component) obj).transform;
    }

    private static T GetComponent<T>(Object obj) where T : Component
    {
        if (obj is GameObject gameObject)
        {
            return gameObject.GetComponent<T>();
        }
        return ((Component) obj).GetComponent<T>();
    }
}

public static class ObjectExtensions
{
    public static Script script(this GameObject gameObject, bool newGameActionChain = true)
    {
        return new Script(gameObject,  OnScriptFinished.Loop, newGameActionChain);
    }
    
    public static Script script(this Component component)
    {
        return new Script(component.gameObject);
    }
}